# -*- coding: utf-8 -*-
"""
Created on Fri Feb 02 00:31:29 2019

@author: Trolldemort
"""

#Code to compute time evolution using unitary gates
import numpy as np
import math
from iter-prun import iter-prune
from vidal-conv import to-mixed

# TOY IMPLEMENTATION (COMMENTED OUT)

# #function which performs the contractions
# #uses the Einstein summation, documentation is at : https://docs.scipy.org/doc/numpy-1.13.0/reference/generated/numpy.einsum.html
# def contract(T0, T1, T2, T3, T4, T5):
#     T6 = np.einsum(T0, (0, 1, 2), T1, (2, 3, 4), T2, (4, 5, 6), T3, (7, 8, 9, 1), T4, (9, 10, 11, 3), T5, (11, 12, 13, 5), (0, 6, 7, 8, 10, 12, 13))
#     return T6

# #initializes some random values to check
# #ensures that corresponding indices on each site have same size
# T0 = np.arange(60).reshape(3,4,5)
# T1 = np.arange(60).reshape(5,4,3)
# T2 = np.arange(60).reshape(3,4,5)
# T3 = np.arange(240).reshape(3,4,5,4)
# T4 = np.arange(240).reshape(5,4,3,4)
# T5 = np.arange(240).reshape(3,4,5,4)

# #calls contraction function
# T6 = contract(T0, T1, T2, T3, T4, T5)

# print(T6)
# print("")
# print("Dimensions are:")
# print(T6.shape)


# ACTUAL IMPLEMENTATION

# ASSUMPTION 1 : must input dimensionally accurate matrices to fill initial MPS list
# ASSUMPTION 2 : n > 2
# ASSUMPTION 3 : all data points are real
# ASSUMPTION 4 : the network structure will not need to be changed

#DO NOT EDIT
#function to compute and then contract dimension accurate network for one time step
def contr_one_tstep(n, vmat, diag gate):
	time_evol = []
	sz = len(vmat)_
	mps = to-mixed(vmat, sz-1 , diag)
	#even number of sites
	if(n%2 == 0):
		#leftmost contraction
		l_contr = np.einsum(mps[0], (0,1), gate, (2,3,0), (1,2,3))
		dim1 = gate.shape[0]
		dim2 = gate.shape[1] * mps[0].shape[1]
		l_contr = l_contr.reshape(dim1, dim2)
		time_evol.append(l_contr)
		#rightmost contraction
		r_contr = np.einsum(mps[len(mps)-1], (0,1), gate, (1,2,3), (0,2,3))
		dim1r = mps[len(mps)-1].shape[0] * gate.shape[1]
		dim2r = mps[len(mps)-1].shape[1]
		r_contr = r_contr.reshape(dim1r, dim2r)
		#contract all middle sites properly
		for i in range(n-2):
			if(i%2 == 0):
				m_contr = np.einsum(mps[i+1], (0,1,2), gate, (1,3,4), gate, (4,5,6), (0,2,3,5,6))
				dim1 = mps[i+1].shape[0] * gate.shape[1]
				dim2 = gate.shape[1]
				dim3 = gate.shape[2] * mps[i+1].shape[2]
				m_contr.reshape(dim1, dim2, dim3)
				time_evol.append(m_contr)
			else:
				m_contr = np.einsum(mps[i+1], (0,1,2), gate, (1,3,4), gate, (3,5,6), (0,2,4,5,6))
				dim1 = mps[i+1].shape[0] * gate.shape[1]
				dim2 = gate.shape[2]
				dim3 = mps[i+1].shape[2] * gate.shape[2]
				m_contr.reshape(dim1, dim2, dim3)
				time_evol.append(m_contr)
		time_evol.append(r_contr)
		return time_evol
	#odd number of sites
	else:
		#leftmost contraction
		l_contr = np.einsum(mps[0], (0,1), gate, (0,2,3), (1,2,3))
		dim1 = gate.shape[1]
		dim2 = mps[0].shape[1] * gate.shape[2]
		l_contr.reshape(dim1, dim2)
		time_evol.append(l_contr)
		#rightmost contraction
		r_contr = np.einsum(mps[len(mps)-1], (0,1), gate, (1,2,3), (0,2,3))
		dim1r = mps[len(mps)-1].shape[0] * gate.shape[1]
		dim2r = gate.shape[2]
		r_contr.reshape(dim1, dim2)
		#contract middle sites properly
		for i in range(n-2):
			if(i%2 == 0):
				m_contr = np.einsum(mps[i+1], (0,1,2), gate, (1,3,4), gate, (4,5,6), (0,2,3,5,6))
				dim1 = mps[i+1].shape[0] * gate.shape[1]
				dim2 = gate.shape[1]
				dim3 = gate.shape[2] * mps[i+1].shape[2]
				m_contr.reshape(dim1, dim2, dim3)
				time_evol.append(m_contr)
			else:
				m_contr = np.einsum(mps[i+1], (0,1,2), gate, (1,3,4), gate, (3,5,6), (0,2,4,5,6))
				dim1 = mps[i+1].shape[0] * gate.shape[1]
				dim2 = gate.shape[2]
				dim3 = mps[i+1].shape[2] * gate.shape[2]
				m_contr.reshape(dim1, dim2, dim3)
				time_evol.append(m_contr)
		time_evol.append(r_contr)
		return time_evol

#list to store all matrix product states
coeff_mat = []

#inputs initial number of sites
n = int(input("Num sites: "))


#random test case where all indices = 2, initializes mps for test
#for custom usage initialize left and right and middle mps matrices, as well as the gate matrix, and append mps to the mps list

#mps matrices (legs = 2, 3, 3, ......, 3, 3, 2)
left_mps = np.arange(4).reshape(2, 2)
right_mps = np.arange(4).reshape(2, 2)

mps.append(left_mps)

for i in range(n-2):
	middle_mps = np.arange(8).reshape(2,2,2)
	mps.append(middle_mps)

mps.append(right_mps)

#gate matrix (legs = 3)
gate = np.arange(8).reshape(2,2,2)

# MPS - > VIDAL ? 

guess = []
one_tstep = contr_one_tstep(n, mps, gate)

#print(one_tstep)
trunc_mps = iter-prune(vmat, diag, guess)
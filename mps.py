# -*- coding: utf-8 -*-
"""
Created on Mon Dec 31 14:50:29 2018

@author: Trolldemort
"""

#Code to decompose wavefunction using MPS
import numpy as np
import math

#inputs initial variables
l = int(input("Num sites: "))
d = int(input("Num possible states: "))
#l = 3
#d = 2

#converts an integer to a string in any base
def toStr(n,base):
   converted = "0123456789ABCDEF"
   if n < base:
      return converted[n]
   else:
      return toStr(n//base,base) + converted[n%base]

#sets rows and cols for coefficient matrix
rows = d
cols = pow(d, l-1)

#assigns random values to coefficient matrix, normalises
coeff_mat = np.random.rand(rows, cols)
ssq = np.sum(coeff_mat**2)
rssq = pow(ssq, 0.5)
coeff_mat /= rssq
#print(coeff_mat)

#required list of matrices for every site
matrix_list = []

#initialises matrix to be decomposed
to_dec = coeff_mat

#iteratively decompose using SVD
for i in range(l-1):
	curr_u, curr_s, curr_vh = np.linalg.svd(to_dec, full_matrices = False)
	curr_m = np.matmul(np.diag(curr_s), curr_vh)
	#print(curr_m.shape)
	curr_list = np.split(curr_u, d)
	#print(curr_list)
	matrix_list.append(curr_list)
	rows = curr_m.shape[0]
	cols = curr_m.shape[1]
	rows = rows*d
	cols = cols/d
	#print("...")
	#print(curr_m)
	#to_dec = np.reshape(curr_m, (rows, cols))
	#print(to_dec)
	#print("...")
	new_mats_list = np.split(curr_m, d, axis = 1)
	to_dec = np.vstack(new_mats_list)


#print(to_dec.shape)

#special case for last column
ssq = np.sum(to_dec**2)
rssq = pow(ssq, 0.5)
to_dec /= rssq
curr_list = np.split(to_dec, d)
matrix_list.append(curr_list)

#array for storing computed coeffs
computed_coeffs = []

#print all coefficients as matrix products recursively, append to list of coefficients
limit = pow(d, l)
for i in range(limit):
	#print(i)
	in_base_d = int(toStr(i, d))
	output_mat = matrix_list[0][in_base_d%10]
	for j in range(l-1):
		k = (in_base_d/pow(10,j+1))%10
		output_mat = np.matmul(output_mat, matrix_list[j+1][k])
	#print(output_mat[0, 0])
	computed_coeffs.append(output_mat[0, 0])

#break list into structure similar to original matrix
coeffs_split_list = np.split(np.array(computed_coeffs), d)
coeff_mat_2 = np.transpose(np.vstack(coeffs_split_list))

#display both matrices for comparision
print('Initial mat is:')
print(coeff_mat)
print('Computed mat is:')
print(coeff_mat_2)

#would check if matrices are equal, if not for floating point anomalies
# if(np.array_equal(coeff_mat, coeff_mat_2)):
# 	print("Yay")
# else:
# 	print("Nay")

#for querying
#for i in range(100):
#	test = int(input("Site number:"))
#	print(matrix_list[test-1])






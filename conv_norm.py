# -*- coding: utf-8 -*-
"""
Created on Fri Feb 15 13:50:29 2019

@author: Trolldemort
"""

#Code to decompose wavefunction using MPS
import numpy as np
import math

#inputs initial variables
l = int(input("Num sites: "))
d = int(input("Num possible states: "))
#l = 3
#d = 2

#convert left-norm to right-norm
def ltor(lnorm):
	rnorm = []
	n = int(len(lnorm))
	for i in range(n):
		j = n-1-i
		if (j != 0):
			u, s, vh = np.linalg.svd(lnorm[j], full_matrices = False)
			s = np.diag(s)
			rnorm.append(vh)
			#print(j)
			temp_mat = np.matmul(u, s)
			lnorm[j-1] = np.matmul(lnorm[j-1], temp_mat)
		else:
			rnorm.append(lnorm[0])
	rnorm.reverse()
	return rnorm

#convert right-norm to left-norm
def rtol(rnorm):
	lnorm = []
	n = int(len(rnorm))
	for i in range(n):
		if (i != n-1):
			u, s, vh = np.linalg.svd(rnorm[i], full_matrices = False)
			s = np.diag(s)
			lnorm.append(u)
			#print(j)
			temp_mat = np.matmul(s, vh)
			rnorm[i+1] = np.matmul(np.transpose(rnorm[i+1]), np.transpose(temp_mat))
			rnorm[i+1] = np.transpose(rnorm[i+1])
		else:
			lnorm.append(rnorm[n-1])
	return rnorm
#sets rows and cols for coefficient matrix
rows = d
cols = pow(d, l-1)

#assigns random values to coefficient matrix, normalises
coeff_mat = np.random.rand(rows, cols)
ssq = np.sum(coeff_mat**2)
rssq = pow(ssq, 0.5)
coeff_mat /= rssq
#print(coeff_mat)

#required list of matrices for every site
matrix_list = []

#initialises matrix to be decomposed
to_dec = coeff_mat

#iteratively decompose using SVD
for i in range(l-1):
	curr_u, curr_s, curr_vh = np.linalg.svd(to_dec, full_matrices = False)
	curr_m = np.matmul(np.diag(curr_s), curr_vh)
	#print(curr_m.shape)
	curr_list = np.split(curr_u, d)
	#print(curr_list)
	matrix_list.append(curr_list)
	rows = curr_m.shape[0]
	cols = curr_m.shape[1]
	rows = rows*d
	cols = cols/d
	#print("...")
	#print(curr_m)
	#to_dec = np.reshape(curr_m, (rows, cols))
	#print(to_dec)
	#print("...")
	new_mats_list = np.split(curr_m, d, axis = 1)
	to_dec = np.vstack(new_mats_list)


#print(to_dec.shape)

#special case for last column
ssq = np.sum(to_dec**2)
rssq = pow(ssq, 0.5)
to_dec /= rssq
curr_list = np.split(to_dec, d)
matrix_list.append(curr_list)

m_list = []
for i in range(len(matrix_list)):
	temp = np.concatenate(matrix_list[i])
	m_list.append(temp)

#print('Initial matrix:')
#print(matrix_list)
ltor_list = ltor(m_list)
print('Left to Right:')
print(ltor_list)
rtol_list = rtol(ltor_list)
print('Right to Left:')
print(rtol_list)




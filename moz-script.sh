# the rust compiler
sudo apt  install rustc

# the rust package manager
sudo apt  install cargo

# the required (old) version of autoconf
sudo apt  install autoconf2.13

# the headers of important libs
sudo apt  install libgtk-2-dev
sudo apt  install libgtk-3-dev
sudo apt  install libgconf2-dev
sudo apt  install libdbus-glib-1-dev
sudo apt  install libpulse-dev

# rust dependencies
cargo install cbindgen

# an assembler for compiling webm
sudo apt  install yasm
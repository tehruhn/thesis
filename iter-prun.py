# -*- coding: utf-8 -*-
"""
Created on Sat Mar 09 15:13:29 2019

@author: Trolldemort
"""

#Code to iteratively prune a given MPS
import numpy as np
import math
from vidal-conv import vidal-conv

#Function to iteratively prune an MPS
def iter-prune(vmps, diag, guess):
	#put guess here
	guess = []
	#number of sites
	n = len(mps)
	#iteratively prune site
	pr-mps = []
	#temporary list
	templist = []
	#evaluates the tensor network site by site
	for j in range(n):
		templist = []
		#builds chain network
		#first builds 
		for i in range(n):
			if(i == j):
				templist.append(mps[j])
			elif(i == j-1 && i != 1 && i != 0):
				newmat = np.einsum(mps[i], (0,1,2), guess[i], (1,3,4), (0,2,3,4))
				dim1 = mps[i].shape[0] * guess[i].shape[2]
				dim2 = mps[i].shape[2] 
				dim3 = guess[i].shape[0]
				newmat.reshape(dim1, dim2, dim3)
				templist.append(newmat)
			elif(i == j+1 && i != 1 && i != 0):
				newmat = np.einsum(mps[i], (0,1,2), guess[i], (1,3,4), (0,2,3,4))
				dim1 = mps[i].shape[0] * guess[i].shape[2]
				dim2 = mps[i].shape[2] 
				dim3 = guess[i].shape[0]
				newmat.reshape(dim1, dim2, dim3)
				templist.append(newmat)
			elif(i == 0):
				newmat = np.einsum(mps[0], (0,1), guess[0], (2,0), (1,2))
				dim1 = mps[0].shape[1] * guess[0].shape[0]
				newmat.reshape(dim1)
				templist.append(newmat)
			elif(i < n-1):
				newmat = np.einsum(mps[i], (0,1,2), guess[i], (1,3,4), (0,2,3,4))
				dim1 = mps[i].shape[0] * guess[i].shape[2]
				dim2 = mps[i].shape[2] * guess[i].shape[0]
				templist.append(newmat)
			else:
				newmat = np.einsum(mps[n-1], (0,1), guess[n-1], (1,2), (0,2))
				dim1 = mps[n-1].shape[0] * guess[n-1].shape[1]
				newmat.reshape(dim1)
				templist.append(newmat)
		#evaluates chain network
		newmat = []
		for i in range(n-1):
			if(i == 0):
				newmat = np.einsum(templist[0], (0), templist[1], (0,1), (1))
			elif(i == n-2):
				newmat = np.einsum(templist[i], (0,1), templist[i+1], (1), (0))
			elif(i != j-1 && i != j):
				newmat = np.einsum(newmat, (0), templist[i+1], (0,1), (1))
			elif(i != j-1):
				newmat = np.einsum(newmat, (0,1), templist[i+1], (1,2), (0,2))
			else:
				newmat = np.einsum(newmat, (0,1), templist[i+1], (1,2,3), (0,2,3))
		pr-mps.append(newmat)
	return pr-mps
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 20 17:04:58 2018

@author: Trolldemort
"""

from scipy.linalg import expm, sinm, cosm
a = np.array([[1.0, 2.0], [-1.0, 3.0]])
print(expm(1j*a))
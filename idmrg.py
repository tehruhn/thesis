# -*- coding: utf-8 -*-
"""
Created on Tue Jan 1 15:05:29 2019

@author: Trolldemort
"""


#code for infinite DMRG

import numpy as np
from numpy import linalg as LA
import math

#computes reduced density matrix given number of states in subset
def rdmCompute(start_mat, choice, hl):
	#n is the size of the final matrix
	vals_list = 0
	sz = start_mat.shape[0]
	n = int(pow(sz, 0.5))
	x = 0
	y = 0
	step = sz/n
	# print("Step")
	# print(step)
	#trace out A
	if(choice == 1):
		vals_list = []
		for i in range(step):
			for j in range(step):
				basex = x + (j*n)
				basey = y + (i*n)
				sum = 0
				for k in range(n):
					sum += start_mat[basex+k, basey+k]
				vals_list.append(sum)
		split_list = np.split(np.array(vals_list), n)
		pa = np.vstack(split_list)
		return pa
	#trace out B
	else:
		ans = np.zeros((n, n), dtype = complex)
		for i in range(step):
			stptxy = x + (i*n) 
			mat = start_mat[stptxy:stptxy+n, stptxy:stptxy+n]
			ans += mat
		return ans


#makes identity matrix of required size
def makeIdm(n):
	idm = np.zeros((n, n))
	for i in range(n):
		idm[i, i] = 1
	return idm



#inputs initial variables
l = int(input("Num sites: "))
#since operators are not known for d != 2 states
d = 2
D = int(input("Cutoff dimension: "))
#l = 4
#D = 2



#spin operators
sx = np.zeros((2, 2),dtype = complex)
sy = np.zeros((2, 2),dtype = complex)
sz = np.zeros((2, 2),dtype = complex)

sx[0, 1] = 1
sx[1, 0] = 1

sy[0, 1] = 0-1j
sy[1, 0] = 0+1j

sz[0, 0] = 1
sz[1, 1] = -1

#identity matrix (2x2)
identity = np.zeros((2,2),dtype = complex)
identity[0, 0] = 1
identity[1, 1] = 1



#variables from previous computations
prev_ha = identity
#basis from previous iteration
ua = identity
ub = identity
hamiltonian = identity
truncate = False



##################### TODO take care of odd L also ####################

#for even L:
for i in range(l/2 + 1):

	#avoiding confusing base case
	if(i == 0):
		continue


	#assumption : will not need to truncate in the very first iteration
	#for initializing, for first two sites only
	elif(i == 1):
		#computes ha
		ha_1 = sx
		ha = np.kron(sx, identity)
		#computes hb
		hb_1 = sx
		hb = np.kron(identity, sx)
		#computes hab
		hab = np.kron(sz, sz)
		hamiltonian = ha + hb + hab
		# if(hamiltonian.shape[0] > D):
		# 	truncate = True
		prev_ha = hamiltonian
		#finds eigenvalues
		w, v = LA.eigh(hamiltonian)
		#find minimum eigenvalue
		gs_idx = np.argmin(w)
		gs_evec = v[:, gs_idx]
		#computes the density matrix
		dens_mat = np.outer(gs_evec, gs_evec)
		#trace out A
		pa = rdmCompute(dens_mat, 1, i)
		#trace out B
		pb = rdmCompute(dens_mat, 2, i)
		#compute eigenvalues and eigenvectors from RDMs
		paw, pav = LA.eigh(pa)
		pbw, pbv = LA.eigh(pb)
		#setting variables for next iteration
		prev_ha = ha_1
		prev_hb = hb_1
		ua = pav
		ub = pbv
		
		print("")
		print("iteration")
		print(i)
		print("hamiltonian")
		print(hamiltonian.shape)
		print("prev_ha")
		print(prev_ha.shape)
		print("uashape")
		print(ua.shape)
		print("")

	#for cases where n > 1
	else:	

		print("")
		print("iteration")
		print(i)
		#need to compute properly sized matrices by kron with identity
		#identity for half of current system
		id_size = pow(2, i)
		if (ua.shape[0] != ua.shape[1]):
			id_size = 2*D

		#identity for 1 less than half of system
		id_size2 = pow(2, i-1)
		if (ua.shape[0] != ua.shape[1]):
			id_size2 = D	
		# #computes dimension accurate sz
		# dimn = prev_ha.shape[0]
		# resized_sz = sz
		# while(resized_sz.shape[0] < dimn):
		# 	resized_sz = np.kron(identity, resized_sz)

		#computes dimension accurate sz
		dimn = prev_ha.shape[0]/2
		resized_sz = np.kron(makeIdm(dimn), sz)
			
		#computes ha
		ha11 = np.kron(np.matmul(np.conjugate(np.transpose(ua)), np.matmul(prev_ha, ua)), identity)
		ha1 = np.kron(np.matmul(np.conjugate(np.transpose(ua)), np.matmul(prev_ha, ua)), np.kron(identity, makeIdm(id_size)))
		ha2 = np.kron(makeIdm(id_size2), np.kron(sx, makeIdm(id_size)))
		ha3 = np.kron(np.matmul(np.conjugate(np.transpose(ua)), np.matmul(resized_sz, ua)), np.kron(sz, makeIdm(id_size)))
		ha = ha1 + ha2 + ha3
		#computes hb
		hb11 = np.kron(identity, np.matmul(np.conjugate(np.transpose(ub)), np.matmul(prev_hb, ub)))
		hb1 = np.kron(makeIdm(id_size), np.kron(identity, np.matmul(np.conjugate(np.transpose(ub)), np.matmul(prev_hb, ub))))
		hb2 = np.kron(makeIdm(id_size), np.kron(sx, makeIdm(id_size2)))
		hb3 = np.kron(makeIdm(id_size), np.kron(sz, np.matmul(np.conjugate(np.transpose(ub)), np.matmul(resized_sz, ub))))
		hb = hb1 + hb2 + hb3
		#computes hab
		hab = np.kron(makeIdm(id_size2), np.kron(sz, np.kron(sz, makeIdm(id_size2))))
		#computes total hamiltonian 
		print("ha")
		print(ha.shape)
		print("hb")
		print(hb.shape)
		print("hab")
		print(hab.shape)
		hamiltonian = ha + hb + hab
		# print("Ham")
		# print(hamiltonian.shape)
		if(hamiltonian.shape[0] > D):
			truncate = True
		#finds eigenvalues
		w, v = LA.eigh(hamiltonian)
		#find minimum eigenvalue
		gs_idx = np.argmin(w)
		gs_evec = v[:, gs_idx]
		#computes the density matrix
		dens_mat = np.outer(gs_evec, gs_evec)
		#trace out A
		pa = rdmCompute(dens_mat, 1, i)
		#trace out B
		pb = rdmCompute(dens_mat, 2, i)
		# print(pa.shape)
		# print(pb.shape)
		#compute eigenvalues and eigenvectors from RDMs
		paw, pav = LA.eigh(pa)
		pbw, pbv = LA.eigh(pb)
		#setting variables for next iteration
		prev_ha = ha11
		prev_hb = hb11
		ua = pav
		ub = pbv

		print("hamiltonian")
		print(hamiltonian.shape)
		print("prev_ha")
		print(prev_ha.shape)
		print("uashape")
		print(ua.shape)
		print("")

		# #new vars for temp a and b
		# ta = paw
		# tb = pbw
		# temp1 = 5
		# temp2 = 5
		# tempcol = identity
		# #lists for storing columns
		# la = []
		# lb = []

		#checks if truncation needs to be done
		if(truncate == True):
			truncate = False
			# #code for truncation
			# #select D greatest values for A
			# temp1 = ta.argsort()[-1*D:][::-1]
			# # print(ta)
			# # print(temp1)
			# for k in range(D):
			# 	tempcol = pav[:, temp1[k]]
			# 	la.append(tempcol)
			# #select D greatest values for B
			# temp2 = tb.argsort()[-1*D:][::-1]
			# # print(tb)
			# # print(temp2)
			# for k in range(D):
			# 	tempcol = pbv[:, temp2[k]]
			# 	lb.append(tempcol)	
			pav = np.flip(pav, axis = 1)
			pbv = np.flip(pbv, axis = 1)

			ua = pav[:, 0:D]
			ub = pbv[:, 0:D]		

			# #update values of A and B's basis vectors
			# ua = np.stack(la, axis = 1)
			# ub = np.stack(lb, axis = 1)

			print("uadims")
			print(ua.shape)

			print("ubdims")
			print(ub.shape)

print(hamiltonian)


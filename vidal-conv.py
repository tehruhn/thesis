# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 18:30:29 2019

@author: Trolldemort
"""

#Code to decompose wavefunction into Vidal-notation matrices
import numpy as np
import math

def vidal-conv(coeff_mat, l ,d)
	#inputs initial variables
	#l = int(input("Num sites: "))
	#d = int(input("Num possible states: "))
	#l = 3
	#d = 2

	#sets rows and cols for coefficient matrix
	rows = d
	cols = pow(d, l-1)

	#assigns random values to coefficient matrix, normalises
	#coeff_mat = np.random.rand(rows, cols)
	ssq = np.sum(coeff_mat**2)
	rssq = pow(ssq, 0.5)
	coeff_mat /= rssq
	#print(coeff_mat)

	#required list of matrices for every site
	matrix_list = []

	#diagonal matrices list
	diag_mat = []

	#initialises matrix to be decomposed
	to_dec = coeff_mat
	prev_diag = coeff_mat

	#iteratively decompose using SVD
	for i in range(l-1):
		curr_u, curr_s, curr_vh = np.linalg.svd(to_dec, full_matrices = False)
		curr_m = np.matmul(np.diag(curr_s), curr_vh)
		curr_list = np.split(curr_u, d)
		if(i == 0):
			matrix_list.append(curr_list)
			diag_mat.append(np.diag(curr_s))
		else:
			inv_s = np.linalg.inv(prev_diag)
			#t_mat = np.matmul(inv_s, curr_u)
			for j in range(len(curr_list)):
				curr_list[j] = np.matmul(inv_s, curr_list[j])
			matrix_list.append(curr_list)
			diag_mat.append(curr_s)
		rows = curr_m.shape[0]
		cols = curr_m.shape[1]
		rows = rows*d
		cols = cols/d
		new_mats_list = np.split(curr_m, d, axis = 1)
		to_dec = np.vstack(new_mats_list)
		prev_diag = np.diag(curr_s)

	#special case for last column
	ssq = np.sum(to_dec**2)
	rssq = pow(ssq, 0.5)
	to_dec /= rssq
	matrix_list.append(to_dec)

	print(matrix_list)
	print(diag_mat)



#Converts Vidal to mixed MPS form about 'kth' site
def to-mixed(vdl, i, diag):
	#size of MPS
	sz = len(diag) + 1
	#new variable for mixed mps
	mmps = []
	#iteratively computing schmidt decompostions
	for i in range(sz):
		if(i == 0):
			mmps.append[vdl[i]]
		if(i < k):
			temp_mat = vdl[i]
			temp_diag = diag[i-1] 
			for mat in temp_mat:
				mat = np.matmul(temp_diag, mat)
			mmps.append(temp_mat)
		elif(i == k):
			temp_mat = vdl[i]
			d1 = diag[i-1]
			d2 = diag[i]
			for mat in temp_mat:
				mat = np.matmul(d1, np.matmul(mat, d2))
			mmps.append(temp_mat)
		elif(i < sz-1):
			temp_mat = vdl[i]
			temp_diag = diag[i]
			for mat in temp_mat:
				mat = np.matmul(mat, temp_diag)
			mmps.append(temp_mat)
		else:
			mmps.append(vdl[i])
	#final return
	return mmps

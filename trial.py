import numpy as np

rows = 4
cols = 4

coeff_mat = np.random.rand(rows, cols)
print(coeff_mat)

split_mat = np.split(coeff_mat, 2, axis = 1)
print(split_mat)

joined_mat = np.vstack(split_mat)
print(joined_mat)
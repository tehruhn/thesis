// STANDARD TEMPLATE LIBRARY EXAMPLES
// 
// C++ has a standard library for certain templated data structures like stacks, queues and trees
// For a user friendly tutorial visit:
//
// https://www.topcoder.com/community/competitive-programming/tutorials/power-up-c-with-the-standard-template-library-part-1/
//

// automatically includes all libraries (including STL)
#include<bits/stdc++.h>
using namespace std;

int main(){

	// --------------------------------------
	// vector - basically a dynamic array
	cout << "Vector template:" << endl;
	// note again that the angular brackets specify type
	// translation of line 19 : variable called 'v' is a vector of integers
	vector<int> v;
	//adding elements to vector 
	for(int i = 0; i < 10; i++){
		v.push_back(i);
	}
	//printing size of vector
	cout << "Size of vector is: ";
	cout << v.size() << endl;
	//---------------------------------------


	// pair - a pair of any two type of elements
	cout << "Pair template:" << endl;
	// brilliant use of templates
	// example one - a pair of two integers
	pair<int, int> p_int_int;
	p_int_int.first = 11;
	p_int_int.second = 12;
	cout << "first element of example 1: ";
	cout << p_int_int.first << endl;
	cout << "second element of example 1: ";
	cout << p_int_int.second << endl;

	// example two - a pair can also further consist of templated pairs
	// connsists of a pair consisting of - a string, and a pair of integers
	// deconstruct following line carefully
	pair<string, pair<int, int> > p_rand;
	p_rand.first = "Hello!";
	p_rand.second.first = 21;
	p_rand.second.second = 22;
	cout << "first element of example 2: ";
	cout << p_rand.first << endl;
	cout << "second elements of example 2: ";
	cout << p_rand.second.first << endl;
	cout << p_rand.second.second << endl;
	//----------------------------------------

	return 0;
}

// BASIC TEMPLATED FUNCTION
// THE FUNCTION RETURNS VALUES IN WHATEVER RETURN TYPE THE USER SPECIFIES
//
// MOTIVATION:
//
// eg - 
//         int greatest_num(int a, int b);
//
// is defined to return an int for integral inputs
// what if user inputs a float?
// should I write different functions for different input/output data types?


// automatically includes all libraries
#include<bits/stdc++.h>
using namespace std;


// the following function works for all data types
// even user defines ones if you overload '<' properly

// generic placeholder 'tarun' is a stand-on for a usual data type
// 'tarun' is basically just a datatype that hasn't been declared yet
// 'int var' is the same as 'tarun var'


template<typename tarun>
tarun greatest_num(tarun num1, tarun num2){
	if(num1 > num2)
		return num1;
	else
		return num2;
}


// tested in main
int main(){
	// note the usage of the above templated function in the following cases
	// note that angular brackets <> are used for type specification at runtime (always)

	// 1. calling the function for int
	cout << "For int: ";
	cout << greatest_num<int>(5, 8) << endl;
	
	// 2. calling the function for double
	cout << "For double: ";
	cout << greatest_num<double>(5.0, 8.9) << endl;
	
	// 3. calling the function for char
	cout << "For char: ";
	cout << greatest_num<char>('a', 'h') << endl;
	return 0;
}



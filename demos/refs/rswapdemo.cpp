// DEMONSTRATES SWAPPING USING REFERENCES

// automatically includes all libraries
#include<bits/stdc++.h>
using namespace std;

// swap function that uses references
// note that the reference variable is declared in the signature itself
// a_ref and b_ref are now references to the passed variables
void swap_ref_fn(int& a_ref, int& b_ref){
	int temp = a_ref;
	a_ref = b_ref;
	b_ref = temp;
}

int main(){
	int a = 2, b = 3;
	swap_ref_fn(a, b);
	cout << a << " " << b << endl;
	return 0;
}
// DEMONSTRATING REFERENCES

// automatically includes all libraries
#include<bits/stdc++.h>
using namespace std;

int main(){
	int x = 10;

	// declaring reference variable to x
	int& ref = x;

	// changes value to 20
	ref = 20;

	cout << "x = " << x << endl;

	// changes value to 30
	x = 30;
	cout << "ref = " << ref << endl;
	
	return 0;
}
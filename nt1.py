# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 12:40:29 2018

@author: Trolldemort
"""

#Code to show precession in the case with two interacting spins
import numpy as np
from scipy.linalg import expm, sinm, cosm
import matplotlib.pyplot as plt


#inputs initial state
matrix_input = []
print("Input initial state into a 4x1 matrix:")
#rows = int(input("Num rows: "))
#cols = int(input("Num columns: "))
rows = 4
cols = 1
for r in range(rows):
    row = []
    for c in range(cols):
        row.append(complex(input("Value for Row: {} Col: {}\n>>>".format(r+1, c+1))))
    matrix_input.append(row)
matrix_input = np.matrix(matrix_input)
#print(matrix_input)


#input magnetic field
mag_field = []
print("Enter mag field values into 1x3 matrix:")
rows = 1
cols = 3
for r in range(rows):
    row = []
    for c in range(cols):
        row.append(complex(input("Value for Row: {} Col: {}\n>>>".format(r+1, c+1))))
    mag_field.append(row)
mag_field = np.matrix(mag_field)
#print(mag_field)


#what is J?
#what do initial spin values and input states look like?


#separate magnetic field into components for easy use
bx = mag_field[0, 0]
by = mag_field[0, 1]
bz = mag_field[0, 2]


#spin operators
sx = np.zeros((2,2),dtype = complex)
sy = np.zeros((2,2),dtype = complex)
sz = np.zeros((2,2),dtype = complex)

sx[0, 1] = 1
sx[1, 0] = 1

sy[0, 1] = 0-1j
sy[1, 0] = 0+1j

sz[0, 0] = 1
sz[1, 1] = -1

#identity matrix (2x2)
identity = np.zeros((2,2),dtype = complex)
identity[0, 0] = 1
identity[1, 1] = 1
#print(sx)
#print(sy)
#print(sz)
#print(identity)


#time = 0.01 seconds, 1000 iterations
t = 0.01
increments = 0.01


#Computing the hamiltonian as the sum of three matrices
mat1 = (bx*np.kron(sx, identity)) + (by*np.kron(sy, identity)) + (bz*np.kron(sz, identity))
mat2 = (bx*np.kron(identity, sx)) + (by*np.kron(identity, sy)) + (bz*np.kron(identity, sz))
mat1 = -1*mat1
mat2 = -1*mat2
mat3 = (np.kron(sx, sx)) + (np.kron(sy, sy)) + (np.kron(sz, sz))

hamiltonian = mat1 + mat2 + mat3
#print(hamiltonian)

prev_state = matrix_input
curr_state = matrix_input

#exponentiation of the Hamiltonian
eiHdt = expm((-1*1j*t)*hamiltonian)

#Kron product of S with identity
sxid = np.kron(sx, identity)

#list that will contain expectation values 
exp_vals = []


#Compute iteratively for 1000 time quanta
for i in range(1000):
    curr_state = np.matmul(eiHdt, prev_state)
    curr_exp = np.matmul(np.conjugate(np.transpose(curr_state)) ,np.matmul(sxid, curr_state))[0, 0]
    exp_vals.append(curr_exp)
    print("Value on iter no. ", i+1, ":")
    prev_state = curr_state
    print(curr_exp)
    print("\n")
    
#plot generated expectation values to show precession
exp_vals = np.array(exp_vals)   
plt.plot(exp_vals.real)
plt.show()

#print(*exp_vals, sep = ", ")